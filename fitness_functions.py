"""A collection of fitness functions for use as a benchmark test for
various heuristic search implementations for "CS3705 - Final Year
Project".

All functions were derived from the following Wikipedia article:
https://en.wikipedia.org/wiki/Test_functions_for_optimization

Author: James Ford
"""

from math import *

from typing import List


def rastrigin_function(position: List[float]) -> float:
    """Rastrigin fitness function.

    Global minimum: f(0,...,0) = 0
    Search domain: -5.12 <= x[i] <= 5.12

    :param position: The position of the particle.
    :return: The calculated fitness of the given position.
    """

    d = len(position)

    return 10 * d + sum([
        position[i]**2 - 10 * cos(2 * pi * position[i]) for i in range(d)
    ])


def sphere_function(position: List[float]) -> float:
    """Sphere fitness function.

    Global minimum: f(0,...,0) = 0
    Search domain: -INF <= x[i] <= INF

    :param position: The position of the particle.
    :return: The calculated fitness of the given position.
    """

    d = len(position)

    return sum([
        position[i]**2 for i in range(d)
    ])


def ackley_function(position: List[float]) -> float:
    """Ackley fitness function.

    Global minimum: f(0,0) = 0
    Search domain: -5 <= x,y <= 5

    :param position: The position of the particle.
    :return: The calculated fitness of the given position.
    """

    x = position[0]
    y = position[1]

    return -20 * exp(-0.2 * sqrt(0.5 * (x**2 + y**2))) \
        - exp(0.5 * (cos(2 * pi * x) + cos(2 * pi * y))) \
        + e + 20


def beale_function(position: List[float]) -> float:
    """Beale fitness function.

    Global minimum: f(3, 0.5) = 0
    Search domain: -4.5 <= x,y <= 4.5

    :param position: The position of the particle.
    :return: The calculated fitness of the given position.
    """

    x = position[0]
    y = position[1]

    return (1.5 - x + x*y)**2 \
        + (2.25 - x + x*y**2)**2 \
        + (2.625 - x + x*y**3)**2


def goldstein_price_function(position: List[float]) -> float:
    """Goldstein-Price fitness function.

    Global minimum: f(0,-1) = 3
    Search domain: -2 <= x,y <= 2

    :param position: The position of the particle.
    :return: The calculated fitness of the given position.
    """

    x = position[0]
    y = position[1]

    return (
        1 + (x + y + 1)**2 * (19 - 14*x + 3*x**2 - 14*y + 6*x*y + 3*y**2)
    ) * (
        30 + (2*x - 3*y)**2 * (18 - 32*x + 12*x**2 + 48*y - 36*x*y + 27*y**2)
    )


def booth_function(position: List[float]) -> float:
    """Booth fitness function.

    Global minimum: f(1,3) = 0
    Search domain: -10 <= x,y <= 10

    :param position: The position of the particle.
    :return: The calculated fitness of the given position.
    """

    x = position[0]
    y = position[1]

    return (x + 2*y - 7)**2 + (2*x + y - 5)**2


def bukin_n6_function(position: List[float]) -> float:
    """Bukin fitness function N.6.

    Global minimum: f(-10,1) = 0
    Search domain: -15 <= x <= -5, -3 <= y <= 3

    :param position: The position of the particle.
    :return: The calculated fitness of the given position.
    """

    x = position[0]
    y = position[1]

    return 100 * sqrt(abs(y - 0.01*x**2)) + 0.01 * abs(x + 10)


def cross_in_tray_function(position: List[float]) -> float:
    """Cross-in-tray fitness function N.6.

    Global minimum: f(1.34941,-1.34941) = -2.06261,
    f(1.34941,1.34941) = -2.06261,
    f(-1.34941,1.34941) = -2.06261,
    f(-1.34941,-1.34941) = -2.06261

    Search domain: -10 <= x,y <= 10

    :param position: The position of the particle.
    :return: The calculated fitness of the given position.
    """

    x = position[0]
    y = position[1]

    return -0.0001 \
        * (abs(
            sin(x) * sin(y) * exp(abs(100 - (sqrt(x**2 + y**2) / pi)))
        ) + 1)**0.1


def custom_function(position: List[float]) -> float:
    """A custom fitness function to provide comparative results."""

    return position[0] * position[1] ** (1/position[2]+1)
