"""Performs data visualisation on results gathered in run.py.

Results files must be generated through generate_results_files script
in order for plots to be produced.

Author: James Ford
"""

from matplotlib import pyplot as plot

import json
import numpy as np
import scipy.stats as stats

from typing import List

from generate_results_files import (
    impossible_r_pairs_file,
    r_boundaries_file,
    convergence_file,
    pso_parameters_file,
)

FUNCTION = 'rastrigin'

# Leave this False if you have not made
# dedicated folders for each function.
DEDICATED_FUNCTION_FOLDER = False

if DEDICATED_FUNCTION_FOLDER:
    impossible_r_pairs_file =\
        impossible_r_pairs_file.replace('results/', f'results/{FUNCTION}/')
    r_boundaries_file =\
        r_boundaries_file.replace('results/', f'results/{FUNCTION}/')
    convergence_file =\
        convergence_file.replace('results/', f'results/{FUNCTION}/')
    pso_parameters_file =\
        pso_parameters_file.replace('results/', f'results/{FUNCTION}/')

# Configure these values based on data size.
P_STEP = 1
T_STEP = 1
LINE_WIDTH = 0.2
SHADE = 1
DIST_XLIM = 10


def read_in_results() -> (
        List[bool],
        List[int],
        dict,
        List[List[List[float]]],
):
    """Reads in results from files.

    :return: A tuple containing:
        - The True/False values from the configure PSO run.
        - The convergence data for the configure PSO run.
        - The optimised PSO parameters.
        - The lower/upper boundaries of the R2 values.
    """

    # Read in results.
    with open(impossible_r_pairs_file, 'r') as file:
        json_str = file.read()
        json_obj = json.loads(json_str)
        invalid_results = json_obj['results']
        file.close()

    with open(pso_parameters_file, 'r') as file:
        json_str = file.read()
        solution = json.loads(json_str)
        file.close()

    with open(convergence_file, 'r') as file:
        json_str = file.read()
        json_obj = json.loads(json_str)
        convergence = json_obj['results']
        file.close()

    with open(r_boundaries_file, 'r') as file:
        json_str = file.read()
        json_obj = json.loads(json_str)
        r_boundaries = json_obj['results']
        file.close()

    return (
        invalid_results,
        convergence,
        solution,
        r_boundaries,
    )


def generate_plots(
        invalid_results: List[bool],
        convergence: List[int],
        solution: dict,
        r_boundaries: List[List[List[float]]],
) -> None:
    """Generates plots from results files.

    :param invalid_results: The True/False values from the configure
        PSO run.
    :param convergence: The convergence data for configure PSO run.
    :param solution: The optimised PSO parameters.
    :param r_boundaries: The lower/upper boundaries of the R2 values.
    """

    n_t = len(r_boundaries[2:])
    n_p = len(r_boundaries[2])

    def replace_nan_and_inf(arr, val):
        return [r if not (np.isnan(r) or np.isinf(r)) else val for r in arr]

    def exclude_nan_and_inf(arr):
        return [r if not (np.isnan(r) or np.isinf(r)) else 0 for r in arr]

    def boundary_surface_plot(
            boundary, r1, method, method_label, colour=None, magnify_shade=1,
    ):
        boundary_plot = [
            [method(
                [replace_nan_and_inf(t[j:j+P_STEP], 0)
                 for t in boundary[i:i+T_STEP]]
            ) for j in range(0, n_p, P_STEP)]
            for i in range(0, n_t, T_STEP)
        ]

        x = np.arange(0, n_p/P_STEP)
        y = np.arange(0, n_t/T_STEP)
        X, Y = np.meshgrid(x, y)
        Z = np.array(boundary_plot) * magnify_shade

        fig = plot.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot_surface(X, Y, Z, color=colour)
        plot.title(
            f'{method_label.title()} R2 values when R1={r1} for optimised '
            f'PSO configuration \nagainst the {FUNCTION.title()} function'
        )
        ax.set_xlabel('Particle')
        ax.set_ylabel('Timestep (-2)')
        ax.set_zlabel('R2')

        xticklabels = [int(x) for x in np.array(ax.get_xticks()) * P_STEP]
        yticklabels = [int(y) for y in np.array(ax.get_yticks()) * T_STEP]
        zticklabels = np.array(ax.get_zticks()) / magnify_shade

        ax.set_xticklabels(xticklabels)
        ax.set_yticklabels(yticklabels)
        ax.set_zticklabels(zticklabels)

        plot.show()

    def impossible_r_plot(magnify_shade=1):
        invalid_results_3d = [
            [
                magnify_shade if invalid_results[(t*n_p)+p] else 0
                for p in range(n_p)
            ]
            for t in range(n_t)
        ]

        x = np.arange(0, n_p)
        y = np.arange(0, n_t)
        X, Y = np.meshgrid(x, y)
        Z = np.array(invalid_results_3d)

        fig = plot.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot_surface(X, Y, Z)
        plot.title(
            'Impossible R pairs for optimised PSO configuration'
            f'\nagainst the {FUNCTION.title()} function '
            '(%.2f%%)' % (solution['fitness'] / len(invalid_results) * 100)
        )
        ax.set_xlabel('Particle')
        ax.set_ylabel('Timestep (-2)')
        ax.set_zticks([0, magnify_shade])
        ax.set_zticklabels(['False', 'True'])
        ax.set_zlabel('Impossible')
        plot.show()

    def convergence_plot(zero_line=True):
        plot.plot(convergence)
        plot.title(
            'Convergence graph for Simulated Annealing for PSO'
            f'\nconfiguration against the {FUNCTION.title()} function'
        )
        plot.ylabel('Best Fitness')
        plot.xlabel('Iteration')
        if zero_line:
            plot.hlines(y=[0, 1],
                        xmin=0,
                        xmax=len(convergence),
                        zorder=3,
                        colors='#333333')
        plot.show()

    def flat_boundaries_plot(flat_boundary, r1, colour=None):
        plot.plot(flat_boundary, color=colour, linewidth=LINE_WIDTH)
        plot.title(
            f'R2 values when R1={r1} for optimised PSO configuration'
            f'\nagainst the {FUNCTION.title()} function'
        )
        plot.ylabel('R2')
        plot.hlines(y=[0, 1],
                    xmin=0,
                    xmax=len(flat_boundary),
                    zorder=3,
                    linewidth=0.5,
                    colors='#333333')
        plot.show()

    def boundary_dist_plot(
            flat_boundary, r1, colour=None, xlim=None, vline=False,
    ):
        plot.title(
            f'Distribution of R2 values when R1={r1} for optimised'
            f'\nPSO configuration against the {FUNCTION.title()} function'
        )
        h = sorted(exclude_nan_and_inf(flat_boundary))
        hmean = np.mean(h)
        hstd = np.std(h)
        pdf = stats.norm.pdf(h, hmean, hstd)
        plot.plot(h, pdf, color=colour)
        if xlim:
            plot.xlim(xlim)
        if vline:
            plot.vlines(x=[0, 1],
                        ymin=0,
                        ymax=pdf.max(),
                        linewidth=LINE_WIDTH,
                        zorder=3,
                        colors='#333333')
        plot.xlabel('R2')
        plot.show()

    def boundary_diff_plot(diff, diff_type):
        plot.plot(diff, linewidth=LINE_WIDTH)
        plot.title(
            f'{diff_type.title()} difference in R2 values at R1=0 and R1=1 '
            f'for optimised\nPSO configuration against the {FUNCTION.title()} '
            f'function'
        )
        plot.ylabel('Diff')
        plot.show()

    def boundary_diff_dist_plot(diff, diff_type):
        plot.title(
            f'Distribution of {diff_type} difference in R2 values at '
            'R1=0 and R1=1 \nfor optimised PSO configuration against the '
            f'{FUNCTION.title()} function'
        )
        h = sorted(exclude_nan_and_inf(diff))
        hmean = np.mean(h)
        hstd = np.std(h)
        pdf = stats.norm.pdf(h, hmean, hstd)
        plot.plot(h, pdf)
        plot.xlabel('Diff')
        plot.show()

    upper_colour = '#F89924'

    # Get upper and lower boundaries.
    lower_boundary = [[p[0] for p in t] for t in r_boundaries[2:]]
    upper_boundary = [[p[1] for p in t] for t in r_boundaries[2:]]

    # Unpack boundaries into flat arrays.
    flat_lower_boundary = [t[i] for t in lower_boundary for i in range(n_p)]
    flat_upper_boundary = [t[i] for t in upper_boundary for i in range(n_p)]
    boundary_diff = [
        flat_upper_boundary[i] - flat_lower_boundary[i] for i
        in range(len(flat_upper_boundary))
    ]
    abs_boundary_diff = [abs(d) for d in boundary_diff]

    # Impossible R values =====================================================
    impossible_r_plot(magnify_shade=SHADE)

    # Convergence =============================================================
    convergence_plot()

    # Lower boundary surfaces =================================================
    boundary_surface_plot(boundary=lower_boundary,
                          r1=0,
                          method=np.mean,
                          method_label='Mean',
                          magnify_shade=SHADE)
    boundary_surface_plot(boundary=lower_boundary,
                          r1=0,
                          method=np.min,
                          method_label='Min')
    boundary_surface_plot(boundary=lower_boundary,
                          r1=0,
                          method=np.max,
                          method_label='Max')

    # Upper boundary surfaces =================================================
    boundary_surface_plot(boundary=upper_boundary,
                          r1=1,
                          colour=upper_colour,
                          method=np.mean,
                          method_label='Mean',
                          magnify_shade=SHADE)
    boundary_surface_plot(boundary=upper_boundary,
                          r1=1,
                          method=np.min,
                          colour=upper_colour,
                          method_label='Min')
    boundary_surface_plot(boundary=upper_boundary,
                          r1=1,
                          colour=upper_colour,
                          method=np.max,
                          method_label='Max')

    # Flat boundaries =========================================================
    flat_boundaries_plot(flat_boundary=flat_lower_boundary,
                         r1=0)
    flat_boundaries_plot(flat_boundary=flat_upper_boundary,
                         r1=1,
                         colour=upper_colour)

    # Lower boundary distribution =============================================
    boundary_dist_plot(flat_boundary=flat_lower_boundary,
                       r1=0)
    boundary_dist_plot(flat_boundary=flat_lower_boundary,
                       r1=0,
                       xlim=[-DIST_XLIM, DIST_XLIM],
                       vline=True)

    # Upper boundary distribution =============================================
    boundary_dist_plot(flat_boundary=flat_upper_boundary,
                       r1=1,
                       colour=upper_colour)
    boundary_dist_plot(flat_boundary=flat_upper_boundary,
                       r1=1,
                       xlim=[-DIST_XLIM, DIST_XLIM],
                       colour=upper_colour,
                       vline=True)

    # Boundary diff ===========================================================
    boundary_diff_plot(diff=boundary_diff,
                       diff_type='actual')
    boundary_diff_plot(diff=abs_boundary_diff,
                       diff_type='absolute')

    # Boundary diff distribution ==============================================
    boundary_diff_dist_plot(diff=boundary_diff,
                            diff_type='actual')
    boundary_diff_dist_plot(diff=abs_boundary_diff,
                            diff_type='absolute')


if __name__ == '__main__':
    all_results = read_in_results()
    (
        invalid_results,
        convergence,
        solution,
        r_boundaries,
    ) = \
        all_results

    generate_plots(*all_results)
