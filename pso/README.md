# Particle Swarm Optimisation

This repo contains an implementation of Particle Swarm Optimisation (PSO), to
be used primarily for my Final Year Project. This version of PSO will be used 
in an attempt to predict the final 3D form of `polyethylene terephthalate 
hydrolase` (PETase), a protein used to hydrolyze `polyethylene terephthalate` 
(PET).