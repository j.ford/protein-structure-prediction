"""Implementation of Particle Swarm Optimisation (PSO) for "CS3705 -
Final Year Project".

Author: James Ford
"""

from pso import Particle, PSOConfigurationException

from typing import Callable, List


def single_objective_optimisation(
        f: Callable[[List[float]], float],
        search_domain: List[List[float]],
        min_max: int,
        T: int,
        N: int,
        w: float,
        c1: float,
        c2: float,
        v_min: float,
        v_max: float,
) -> (
        List[float],
        float,
        List[Particle],
):
    """Returns a solution for single-objective, particle swarm
    optimisation. Requires configuration of the following parameters:

    :param f: The fitness function used to evaluate the particle
        position.
    :param search_domain: The lower/upper bounds of each dimension in
        the search space. e.g. [[-1,1],[-2,2]] for x to be between -1
        and 1, and y to be between -2 and 2.
    :param min_max: 1 for maximisation problem, -1 for minimisation.
    :param T: The number of iterations PSO should run for.
    :param N: The number of particles in the swarm.
    :param w: The inertia weight. The smaller the value, the faster
        the particles will slow to a crawl.
    :param c1: The cognitive constant. An increased value will add a
        higher weight to the local best position, making all particles
        more inclined to move towards their own best position.
    :param c2: The social constant. An increased value will add a
        higher weight to the global best position, making all particles
        more inclined to move towards it.
    :param v_min: The minimum velocity.
    :param v_max: The maximum velocity.
    :return: A tuple containing the global best position and fitness,
        as well as the swarm, in its final position.
    """

    # Check configuration for errors.
    if min_max != 1 and min_max != -1:
        raise PSOConfigurationException(
            "min_max parameter must be configured to 1 (for a maximisation"
            "problem) or -1 (for a minimisation problem)!"
        )
    if len(search_domain) < 1:
        raise PSOConfigurationException(
            "search_domain parameter must contain at least one dimension!"
        )

    # Initialise the particles.
    particles = [Particle(
        search_domain=search_domain,
        v_min=v_min,
        v_max=v_max,
    ) for _ in range(N)]

    # Initialise the global best position and fitness.
    g_pos = None
    g_fitness = None

    # For the number of iterations...
    for t in range(T):

        # For each particle...
        for p in particles:

            # Calculate and assign best positions and fitness.
            p.best_position, p.best_fitness, g_pos, g_fitness = \
                get_best_positions_and_fitness(
                    p=p,
                    g_pos=g_pos,
                    g_fitness=g_fitness,
                    f=f,
                    min_max=min_max,
                )

            # Adjust the particle's velocity and position.
            p.adjust_velocity(g_pos, w, c1, c2)
            p.adjust_position()

    return g_pos, g_fitness, particles


def get_best_positions_and_fitness(
        p: Particle,
        g_fitness: float,
        g_pos: List[float],
        f: Callable[[List[float]], float],
        min_max: int,
):
    """Assesses a particle's fitness against the particle's current
    best and the global best.

    :param p: The particle to assess.
    :param g_fitness: The current global best fitness.
    :param g_pos: The current global best position.
    :param f: The fitness function used to evaluate the particle
        position.
    :param min_max: 1 for maximisation problem, -1 for minimisation.
    :return: The particle's new best position and fitness, as well as
        the new global best position and fitness.
    """

    # Calculate the fitness of the current position.
    fx = f(p.position)

    # If the fitness is better than the particle's best fitness...
    if p.best_fitness is None \
            or min_max * fx > min_max * p.best_fitness:
        # ...set the new fitness as the best fitness...
        p.best_fitness = fx
        # ...and set the current position as the best position.
        p.best_position = p.position.copy()

    # If the fitness is better than global best fitness...
    if g_fitness is None \
            or min_max * fx > min_max * g_fitness:
        # ...set the new fitness as the global best fitness...
        g_fitness = fx
        # ...and set the current position as the global best position.
        g_pos = p.position.copy()

    return p.best_position.copy(), p.best_fitness, g_pos.copy(), g_fitness
