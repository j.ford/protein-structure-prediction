"""A file of custom exceptions for use in the implementation of
Particle Swarm Optimisation (PSO) for "CS3705 - Final Year Project".

Author: James Ford
"""


class PSOConfigurationException(Exception):
    """An exception generated when PSO has been configured improperly.
    """
    pass
