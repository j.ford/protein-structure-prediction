"""A script for running PSO tests with different configurations and
fitness functions.

Author: James Ford
"""

from datetime import datetime

from fitness_functions import *

from pso import *


if __name__ == '__main__':

    print('Running PSO...')

    # Start the timer.
    start = datetime.now()

    # Run PSO...
    pos, fitness, _ = single_objective_optimisation(
        f=sphere_function,
        search_domain=[[-5.12, 5.12]] * 3,
        min_max=-1,
        T=1000,
        N=1000,
        w=0.8413037011193958,
        c1=1.485702350913026,
        c2=0.21913785520165985,
        v_min=0,
        v_max=1,
    )

    # Record time elapsed.
    time_elapsed = datetime.now() - start

    # Display time and solution.
    print('Done!')

    print(f'\nTime elapsed: {time_elapsed}')

    print(f'\nPosition: {["%.5f" % p for p in pos]}')
    print(f'Fitness: {fitness}')
