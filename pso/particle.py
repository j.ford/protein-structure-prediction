"""Particle object for use in Particle Swarm Optimisation (PSO) for
"CS3705 - Final Year Project".

Author: James Ford
"""

from random import uniform

from typing import List


class Particle:
    """A particle used in PSO. Has an associated position, velocity and
    fitness, as well as the best recorded position and its fitness.
    """

    def __init__(
            self,
            search_domain: List[List[float]],
            v_min: float,
            v_max: float,
    ):
        """Randomly initialise the positions and velocities.

        :param search_domain: The lower/upper bounds of each dimension
            in the search space. e.g. [[-1,1],[-2,2]] for x to be
            between -1 and 1, and y to be between -2 and 2.
        :param v_min: The minimum velocity.
        :param v_max: The maximum velocity.
        """

        self.position = [uniform(d[0], d[1]) for d in search_domain]
        self.velocity = [uniform(v_min, v_max) for _ in search_domain]

        self.best_position = self.position.copy()
        self.best_fitness = None

    def adjust_velocity(
            self,
            g_pos: List[float],
            w: float,
            c1: float,
            c2: float,
    ) -> None:
        """Adjusts the velocity of the particle.

        :param g_pos:  The current best global position.
        :param w: The inertia weight. The smaller the value, the faster
            the particles will slow to a crawl.
        :param c1: The cognitive constant. An increased value will add
            a higher weight to the local best position, making all
            particles more inclined to move towards their own best
            position.
        :param c2: The social constant. An increased value will add a
            higher weight to the global best position, making all
            particles more inclined to move towards it.
        """

        r1 = uniform(0, 1)
        r2 = uniform(0, 1)

        for i in range(len(self.velocity)):
            self.velocity[i] = (self.velocity[i] * w) \
                + (c1 * r1 * (self.best_position[i] - self.position[i])) \
                + (c2 * r2 * (g_pos[i] - self.position[i]))

    def adjust_position(self) -> None:
        """Adjusts the position of the particle based on its new
        velocity.
        """

        for i in range(len(self.position)):
            self.position[i] += self.velocity[i]
