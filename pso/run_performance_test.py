"""A script for running PSO average fitness tests with different
configurations and fitness functions.

Author: James Ford
"""

from datetime import datetime

from fitness_functions import *

from pso import single_objective_optimisation


def performance_test(num_tests: int, num_iter: int):
    """Runs performance test for PSO."""

    print('Running PSO performance test...')

    results = []
    time_results = []
    for test in range(num_tests):
        print(f'\nTest: {test+1} ================================')
        results_for_test = []
        time_results_for_test = []
        for iter in range(num_iter):
            start = datetime.now()
            _, fitness, _ = single_objective_optimisation(
                f=rastrigin_function,
                search_domain=[[-5.12, 5.12]] * 3,
                min_max=-1,
                T=10**iter,
                N=10**iter,
                w=0.8413037011193958,
                c1=1.485702350913026,
                c2=0.21913785520165985,
                v_min=0,
                v_max=1,
            )
            time_elapsed = datetime.now() - start
            results_for_test.append(fitness)
            time_results_for_test.append(time_elapsed.microseconds)
            print(f'{100 ** iter}: {fitness} ({time_elapsed})')

        results.append(results_for_test)
        time_results.append(time_results_for_test)

    return (
        results,
        time_results,
    )


if __name__ == '__main__':
    performance_test(
        num_tests=10,
        num_iter=4,
    )
