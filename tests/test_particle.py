from pso import Particle


class TestParticle:

    def test_particle_initial_position(self):
        search_domain = [[-10, 10]]
        p = Particle(
            search_domain=search_domain,
            v_min=0,
            v_max=1,
        )
        assert len(p.position) == len(search_domain)
        assert search_domain[0][0] < p.position[0] < search_domain[0][1]

    def test_particle_initial_velocity(self):
        search_domain = [[-10, 10]]
        v_min = 0
        v_max = 1
        p = Particle(
            search_domain=search_domain,
            v_min=v_min,
            v_max=v_max,
        )
        assert len(p.velocity) == len(search_domain)
        assert v_min < p.velocity[0] < v_max

    def test_adjust_position(self):
        p = Particle(
            search_domain=[[-10, 10]],
            v_min=0,
            v_max=1,
        )
        initial_position = p.position.copy()
        p.adjust_position()
        for i in range(len(p.position)):
            assert(p.position[i] == initial_position[i] + p.velocity[i])

    def test_adjust_velocity(self):
        p = Particle(
            search_domain=[[-10, 10]],
            v_min=0,
            v_max=1,
        )
        v = p.velocity.copy()
        p.adjust_velocity(
            c1=1,
            c2=2,
            g_pos=[0],
            w=1
        )
        assert(p.velocity != v)
