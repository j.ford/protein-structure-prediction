from fitness_functions import rastrigin_function

from hc import (
    random_mutation as core_random_mutation,
    random_restart as core_random_restart,
    simulated_annealing as core_simulated_annealing,
    BenchmarkProblem,
)


problem = BenchmarkProblem(
    f=rastrigin_function,
    search_domain=[[-5.12, 5.12]]
)


def random_mutation(**kwargs):
    return core_random_mutation(
        f=kwargs['f']
        if 'f' in kwargs else problem.fitness_function,
        random_start=kwargs['random_start']
        if 'random_start' in kwargs else problem.random_start,
        small_change=kwargs['small_change']
        if 'small_change' in kwargs else problem.small_change,
        iterations=kwargs['iterations']
        if 'iterations' in kwargs else 100,
        min_max=kwargs['min_max']
        if 'min_max' in kwargs else -1,
    )


def random_restart(**kwargs):
    return core_random_restart(
        f=kwargs['f']
        if 'f' in kwargs else problem.fitness_function,
        random_start=kwargs['random_start']
        if 'random_start' in kwargs else problem.random_start,
        small_change=kwargs['small_change']
        if 'small_change' in kwargs else problem.small_change,
        iterations=kwargs['iterations']
        if 'iterations' in kwargs else 100,
        min_max=kwargs['min_max']
        if 'min_max' in kwargs else -1,
        iter_rr=kwargs['iter_rr']
        if 'iter_rr' in kwargs else 10,
    )


def simulated_annealing(**kwargs):
    return core_simulated_annealing(
        f=kwargs['f']
        if 'f' in kwargs else problem.fitness_function,
        random_start=kwargs['random_start']
        if 'random_start' in kwargs else problem.random_start,
        small_change=kwargs['small_change']
        if 'small_change' in kwargs else problem.small_change,
        min_max=kwargs['min_max']
        if 'min_max' in kwargs else -1,
        iterations=kwargs['iterations']
        if 'iterations' in kwargs else 100,
        T_0=kwargs['T_0']
        if 'T_0' in kwargs else 100,
        T_iter=kwargs['T_iter']
        if 'T_iter' in kwargs else 0.01,
    )
