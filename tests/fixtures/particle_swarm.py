from fitness_functions import rastrigin_function

from pso import (
    single_objective_optimisation as core_single_objective_optimisation,
)


def single_objective_optimisation(**kwargs):
    return core_single_objective_optimisation(
        f=kwargs['f']
        if 'f' in kwargs else rastrigin_function,
        search_domain=kwargs['search_domain']
        if 'search_domain' in kwargs else [[-5.12, 5.12]],
        min_max=kwargs['min_max']
        if 'min_max' in kwargs else -1,
        T=kwargs['T']
        if 'T' in kwargs else 1,
        N=kwargs['N']
        if 'N' in kwargs else 1,
        w=kwargs['w']
        if 'w' in kwargs else 1,
        c1=kwargs['c1']
        if 'c1' in kwargs else 1,
        c2=kwargs['c2']
        if 'c2' in kwargs else 2,
        v_min=kwargs['v_min']
        if 'v_min' in kwargs else 0,
        v_max=kwargs['v_max']
        if 'v_max' in kwargs else 1,
    )
