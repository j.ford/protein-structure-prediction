from pytest import raises

from pso import *

from tests.fixtures import single_objective_optimisation


class TestParticleSwarm:

    def test_invalid_min_max_switch_throws_exception(self):
        with raises(PSOConfigurationException):
            _ = single_objective_optimisation(
                min_max=2,
            )

    def test_invalid_search_domain_throws_exception(self):
        with raises(PSOConfigurationException):
            _ = single_objective_optimisation(
                search_domain=[],
            )

    def test_particle_number_is_equal_to_N(self):
        N = 10
        _, _, swarm = single_objective_optimisation(
            N=N,
        )
        assert len(swarm) == N

    def test_particle_swarm_returns_solution_fitness_and_swarm(self):
        solution, fitness, swarm = single_objective_optimisation(
            search_domain=[[-5.12, 5.12]] * 3,
            N=10,
        )
        assert type(solution) == list
        for d in solution:
            assert type(d) == float

        assert type(fitness) == float

        assert type(swarm) == list
        for p in swarm:
            assert type(p) == Particle
