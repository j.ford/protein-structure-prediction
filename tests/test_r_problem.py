from random import uniform

from hc import RProblem

from fitness_functions import rastrigin_function

from tests.fixtures import simulated_annealing


class TestRProblem:

    def test_sa_with_r_problem_returns_expected_solution_and_fitness(self):
        n_p = 1
        n_t = 3

        problem = RProblem(
            f=rastrigin_function,
            trajectory=[[[
                uniform(1, 100)
                for _ in range(3)]
                for _ in range(n_p)]
                for _ in range(n_t)],
            N=n_p,
        )

        solution, fitness = simulated_annealing(
            f=problem.fitness_function,
            random_start=problem.random_start,
            small_change=problem.small_change,
            iterations=3,
        )

        assert type(solution) == list
        assert len(solution) == 3
        for param in solution:
            assert type(param) == float

        assert type(fitness) == int
