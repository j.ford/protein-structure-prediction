from pytest import raises

from fitness_functions import rastrigin_function as rastrigin

from hc import *

from tests.fixtures import (
    random_mutation,
    random_restart,
    simulated_annealing,
)


class TestHillClimbers:

    def test_random_mutation_invalid_min_max_switch_throws_exception(self):
        with raises(HCConfigurationException):
            _ = random_mutation(
                min_max=2,
            )

    def test_random_restart_invalid_min_max_switch_throws_exception(self):
        with raises(HCConfigurationException):
            _ = random_restart(
                min_max=2,
            )

    def test_random_restart_invalid_iterations_and_iter_rr_throws_exception(self):
        with raises(HCConfigurationException):
            _ = random_restart(
                iterations=100,
                iter_rr=3,
            )

    def test_simulated_annealing_invalid_min_max_switch_throws_exception(self):
        with raises(HCConfigurationException):
            _ = simulated_annealing(
                min_max=2,
            )

    def test_simulated_annealing_invalid_T_0_and_T_iter_throws_exception(self):
        with raises(HCConfigurationException):
            _ = simulated_annealing(
                T_0=1,
                T_iter=10
            )

    def test_random_mutation_returns_solution_and_fitness(self):
        search_domain = [[-5.12, 5.12]] * 3
        problem = BenchmarkProblem(f=rastrigin, search_domain=search_domain)
        solution, fitness = random_mutation(
            f=problem.fitness_function,
            random_start=problem.random_start,
            small_change=problem.small_change,
        )
        assert type(solution) == list
        assert len(solution) == len(search_domain)
        assert type(solution) == list
        for d in solution:
            assert type(d) == float
            assert d >= -5.12
            assert d <= 5.12

        assert type(fitness) == float

    def test_random_restart_returns_solution_and_fitness(self):
        search_domain = [[-5.12, 5.12]] * 3
        problem = BenchmarkProblem(f=rastrigin, search_domain=search_domain)
        solution, fitness = random_restart(
            f=problem.fitness_function,
            random_start=problem.random_start,
            small_change=problem.small_change,
        )
        assert type(solution) == list
        assert len(solution) == len(search_domain)
        assert type(solution) == list
        for d in solution:
            assert type(d) == float
            assert d >= -5.12
            assert d <= 5.12

        assert type(fitness) == float

    def test_simulated_returns_solution_and_fitness(self):
        search_domain = [[-5.12, 5.12]] * 3
        problem = BenchmarkProblem(f=rastrigin, search_domain=search_domain)
        solution, fitness = simulated_annealing(
            f=problem.fitness_function,
            random_start=problem.random_start,
            small_change=problem.small_change,
        )
        assert type(solution) == list
        assert len(solution) == len(search_domain)
        assert type(solution) == list
        for d in solution:
            assert type(d) == float
            assert d >= -5.12
            assert d <= 5.12

        assert type(fitness) == float
