# PETase Data files

This directory should contain the PETase data files. Due to the data not being
my own, it is omitted from this repository at the present time. That said, some 
details about what the data actually contains:

### TPR File
The TPR file contains topology data for PETase. The file lists the atoms and 
residues (and also their connectivity).

### XTC Files
The XTC files both contain trajectory data, containing a list of coordinates in 
the order defined in the topology. It is a time series of coordinate frames.
