"""A script for running PSO and HC performance tests and plotting them
on some graphs for data analysis.

Author: James Ford
"""

import json

from matplotlib import pyplot as plot

import numpy as np

from typing import List

from generate_performance_files import (
    pso_performance_file,
    sa_performance_file,
    NUM_OF_ITERATIONS,
    NUM_OF_TESTS,
)


def plot_graphs(
        results: List[float],
        time_results: List[float],
        algorithm_name: str,
        fitness_function_name: str,
) -> None:
    """Plots performance test graphs.

    :param results: The convergence results.
    :param time_results: The performance time results.
    :param algorithm_name: The name of the heuristic search algorithm.
        Used in the graph title.
    :param fitness_function_name: The name of the fitness function.
        Used in the graph title.
    """

    average_results = get_average_results(results)
    average_time_results = get_average_results(time_results)

    # Plot fitness results
    x = np.array([100 ** i for i in range(NUM_OF_ITERATIONS)])
    y = np.array(average_results)

    plot.plot(x, y)
    plot.title(
        'Average fitness per iteration for %s against the %s function'
        % (algorithm_name, fitness_function_name)
    )
    plot.xlabel('Number of Iterations')
    plot.xscale('log')
    plot.ylabel('Average Fitness')
    plot.hlines(y=[0],
                xmin=0,
                xmax=1000000,
                colors='#d3d3d3')
    plot.show()

    # Plot time results
    y = np.array(average_time_results)

    plot.plot(x, y)
    plot.title(
        'Average time per iteration for %s against the %s function'
        % (algorithm_name, fitness_function_name)
    )
    plot.xscale('log')
    plot.xlabel('Number of Iterations')
    plot.yscale('log')
    plot.ylabel('Time Elapsed (Microseconds)')
    plot.show()


def get_average_results(results: List[float]) -> List[float]:
    """Returns average results from results.

    :param results: The results to average.
    :return: The average of the results.
    """

    return [
        sum([list(i) for i in zip(*results)][t]) / NUM_OF_TESTS
        for t in range(NUM_OF_ITERATIONS)
    ]


if __name__ == '__main__':
    with open(pso_performance_file, 'r') as file:
        json_str = file.read()
        json_obj = json.loads(json_str)
        pso_fitness = json_obj['fitness_results']
        pso_time = json_obj['time_results']
        file.close()

    with open(sa_performance_file, 'r') as file:
        json_str = file.read()
        json_obj = json.loads(json_str)
        sa_fitness = json_obj['fitness_results']
        sa_time = json_obj['time_results']
        file.close()

    plot_graphs(pso_fitness, pso_time, 'PSO', 'Rastrigin')
    plot_graphs(sa_fitness, sa_time, 'SA', 'Rastrigin')
