"""Script for generating results files used in data analysis.

Author: James Ford
"""

from MDAnalysis import Universe

import json
import sys

from configure_pso import run as configure_run

from fitness_functions import *

from hc.problems.r import RProblem

data_dir = 'data'
results_dir = 'results'

topology_file = f'{data_dir}/CA-PETase.tpr'
trajectory_file = f'{data_dir}/500ns-CA-1-PETase.xtc'

impossible_r_pairs_file = f'{results_dir}/impossible_r.json'
r_boundaries_file = f'{results_dir}/r_boundaries.json'
convergence_file = f'{results_dir}/convergence.json'
pso_parameters_file = f'{results_dir}/pso_parameters.json'

LOWER_BOUND = 0
UPPER_BOUND = 10
ITERATIONS = 100
FITNESS_FUNCTION = rastrigin_function


def output_files(
        lower_bound: int = LOWER_BOUND,
        upper_bound: int = UPPER_BOUND,
        iterations: int = ITERATIONS,
):
    """Runs SA on the R problem and outputs results to files."""

    universe = Universe(topology_file, trajectory_file)

    problem = RProblem(
        f=rastrigin_function,
        p_max=2,
        trajectory=universe.trajectory[lower_bound:upper_bound],
    )

    solution, fitness = configure_run(
        problem=problem,
        iterations=iterations,
    )

    with open(impossible_r_pairs_file, 'w') as file:
        output = json.dumps({
            'results': problem.best_results,
        })
        file.write(output)
        file.close()

    with open(r_boundaries_file, 'w') as file:
        output = json.dumps({
            'results': problem.boundaries,
        })
        file.write(output)
        file.close()

    with open(convergence_file, 'w') as file:
        output = json.dumps({
            'results': problem.best_fitness_per_iter,
        })
        file.write(output)
        file.close()

    with open(pso_parameters_file, 'w') as file:
        output = json.dumps({
            'w': solution[0],
            'c1': solution[1],
            'c2': solution[2],
            'fitness': fitness,
        })
        file.write(output)
        file.close()


if __name__ == '__main__':
    args = sys.argv[1:]
    n_args = len(args)

    lower = int(args[0]) if n_args > 0 else None
    upper = int(args[1]) if n_args > 1 else None
    iterations = int(args[2]) if n_args > 2 else None

    output_files(
        lower or LOWER_BOUND,
        upper or UPPER_BOUND,
        iterations or ITERATIONS,
    )
