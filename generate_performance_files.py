"""A script for running PSO and HC performance tests and outputting
them to json files.

Author: James Ford
"""

import json

from pso import performance_test as pso_performance

from hc import performance_test as sa_performance

NUM_OF_TESTS = 25
NUM_OF_ITERATIONS = 4

results_dir = 'results'

pso_performance_file = f'{results_dir}/pso_performance.json'
sa_performance_file = f'{results_dir}/sa_performance.json'


if __name__ == '__main__':
    pso_fitness, pso_time = pso_performance(NUM_OF_TESTS, NUM_OF_ITERATIONS)
    with open(pso_performance_file, 'w') as file:
        output = json.dumps({
            'fitness_results': pso_fitness,
            'time_results': pso_time,
        })
        file.write(output)
        file.close()

    sa_fitness, sa_time = sa_performance(NUM_OF_TESTS, NUM_OF_ITERATIONS)
    with open(sa_performance_file, 'w') as file:
        output = json.dumps({
            'fitness_results': sa_fitness,
            'time_results': sa_time,
        })
        file.write(output)
        file.close()
