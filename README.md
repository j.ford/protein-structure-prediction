# Protein Structure Prediction

This repo's primary purpose is to be used in an attempt to predict the final 3D 
form of `polyethylene terephthalate hydrolase` (PETase), a protein used to 
hydrolyze `polyethylene terephthalate` (PET). Using a particle swarm based,
whose initial positions will be mapped to the initial topology of PETase, found
in the data, we will attempt to mimic the trajectory of the atoms and produce
a swarm that will resemble the final atomic positions.

### Installing Python packages

The necessary python packages can be found in `requirements.txt`.

### HC

The HC module contains implementation for various hill climbing algorithms, to
be used to find optimum configurations for PSO.

### PSO

The PSO module contains the implementation for particle swarm optimisation.
