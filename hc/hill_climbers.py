"""Implementation of Hill Climbing Optimisation (HC) for "CS3705 -
Final Year Project".

Author: James Ford
"""

from hc.exceptions import HCConfigurationException

from math import exp

from random import uniform

from typing import Any, Callable


def random_mutation(
        f: Callable[[Any], float],
        random_start: Callable[[], Any],
        small_change: Callable[[Any], Any],
        iterations: int,
        min_max: int,
) -> (Any, float):
    """A generalisable implementation of a Random Mutation Hill
    Climber.

    :param f: The fitness function used to evaluate a solution.
    :param random_start: A function that generates a random starting
        solution.
    :param small_change: A function that perform a small change
        operation on a given solution.
    :param iterations: The number of iterations (or fitness function
        calls) the hill climber should run for (or perform).
    :param min_max: 1 for maximisation problem, -1 for minimisation.
    :return: The solution and its fitness.
    """

    # Check configuration for errors.
    if min_max != 1 and min_max != -1:
        raise HCConfigurationException(
            "min_max parameter must be configured to 1 (for a maximisation"
            "problem) or -1 (for a minimisation problem)!"
        )

    # Generate an initial random solution...
    solution = random_start()
    # ...and calculate its fitness.
    fitness = f(solution)

    # For the number of iterations... (minus 1, because we just generated a
    # solution!)
    for i in range(iterations - 1):
        # Perform a small change operation and store it as the new solution.
        new_solution = small_change(solution)
        # Calculate the new fitness.
        new_fitness = f(new_solution)
        # If the new solution is better than the last...
        if min_max * new_fitness > min_max * fitness:
            # ...set it as the current solution.
            solution = new_solution.copy() \
                if type(solution) == list else new_solution
            fitness = new_fitness

    # Return the solution and its fitness.
    return solution, fitness


def random_restart(
        f: Callable[[Any], float],
        random_start: Callable[[], Any],
        small_change: Callable[[Any], Any],
        iterations: int,
        min_max: int,
        iter_rr: int = 10,
) -> (Any, float):
    """A generalisable implementation of a Random Restart Hill
    Climber.

    :param f: The fitness function used to evaluate a solution.
    :param random_start: A function that generates a random starting
        solution.
    :param small_change: A function that perform a small change
        operation on a given solution.
    :param iterations: The number of iterations (or fitness function
        calls) the hill climber should run for (or perform).
    :param min_max: 1 for maximisation problem, -1 for minimisation.
    :param iter_rr: The number of iterations (or random mutation
        solutions) the restart climber will run for (or create).
        The random restart hill climber will generate iter_rr solutions
        that will each run for iter_total/iter_rr iterations. Default
        is 10.
    :return: The solution and its fitness.
    """

    # Check configuration for errors.
    if min_max != 1 and min_max != -1:
        raise HCConfigurationException(
            "min_max parameter must be configured to 1 (for a maximisation"
            "problem) or -1 (for a minimisation problem)!"
        )
    if iterations % iter_rr != 0:
        raise HCConfigurationException(
            "iterations parameter must be divisible by the iter_rr parameter!"
        )

    # Run an initial random mutation hill climber for the starting solution
    # and fitness.
    solution, fitness = random_mutation(
        f=f,
        random_start=random_start,
        small_change=small_change,
        iterations=int(iterations/iter_rr),
        min_max=min_max,
    )

    # For the number of iterations... (minus 1, because we just generated a
    # solution!)
    for i in range(iter_rr - 1):
        # Run another random mutation hill climber and store its solution
        # and fitness.
        new_solution, new_fitness = random_mutation(
            f=f,
            random_start=random_start,
            small_change=small_change,
            iterations=int(iterations/iter_rr),
            min_max=min_max,
        )
        # If the new solution is better than the last...
        if min_max * new_fitness > min_max * fitness:
            # ...set it as the current solution.
            solution = new_solution.copy() \
                if type(solution) == list else new_solution
            fitness = new_fitness

    # Return the solution and its fitness.
    return solution, fitness


def simulated_annealing(
        f: Callable[[Any], float],
        random_start: Callable[[], Any],
        small_change: Callable[[Any], Any],
        min_max: int,
        iterations: int,
        T_0: float,
        T_iter: float,
) -> (Any, float):
    """A generalisable implementation of a Simulated Annealing Hill
    Climber.

    :param f: The fitness function used to evaluate a solution.
    :param random_start: A function that generates a random starting
        solution.
    :param small_change: A function that perform a small change
        operation on a given solution.
    :param min_max: 1 for maximisation problem, -1 for minimisation.
    :param iterations: The number of iterations (or fitness function
        calls) the hill climber should run for (or perform).
    :param T_0: The starting temperature.
    :param T_iter: The final temperature. Should be a small value (e.g.
        0.001).
    :param min_max: 1 for maximisation problem, -1 for minimisation.
    :return: The solution and its fitness.
    """

    # Check configuration for errors.
    if min_max != 1 and min_max != -1:
        raise HCConfigurationException(
            "min_max parameter must be configured to 1 (for a maximisation"
            "problem) or -1 (for a minimisation problem)!"
        )
    if T_0 < T_iter:
        raise HCConfigurationException(
            "T_0 parameter must be higher than the T_iter parameter!"
        )

    cooling_rate = (T_iter / T_0)**(1/iterations)

    # Generate an initial random solution...
    solution = random_start()
    # ...and calculate its fitness.
    fitness = f(solution)

    # Set T_i to the starting temperature.
    T_i = T_0

    # For the number of iterations... (minus 1, because we just generated a
    # solution!)
    for i in range(iterations - 1):
        # Perform a small change operation and store it as the new solution.
        new_solution = small_change(solution)
        # Calculate the new fitness.
        new_fitness = f(new_solution)

        # If the new solution is better than the last **OR** our acceptance
        # probability is higher than the threshold...
        if min_max * new_fitness > min_max * fitness \
                or exp(-abs(new_fitness - fitness) / T_i) > uniform(0, 1):
            # ...set it as the current solution.List[bool]
            solution = new_solution.copy() \
                if type(solution) == list else new_solution
            fitness = new_fitness

        # Update the temperature.
        T_i *= cooling_rate

    # Return the solution and its fitness.
    return solution, fitness
