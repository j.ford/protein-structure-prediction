"""A script for running HC tests with different configurations and
problems.

Author: James Ford
"""

from datetime import datetime

from fitness_functions import *

from hc.hill_climbers import *
from hc.problems import *


if __name__ == '__main__':

    # Set the problem.
    p = BenchmarkProblem(
        f=sphere_function,
        search_domain=[[-5.12, 5.12]] * 3,
    )

    print('Running HC...')

    # Start the timer.
    start = datetime.now()

    solution, fitness = simulated_annealing(
        f=p.fitness_function,
        random_start=p.random_start,
        small_change=p.small_change,
        min_max=-1,
        iterations=1000000,
        T_0=100,
        T_iter=0.01,
    )

    # Record time elapsed.
    time_elapsed = datetime.now() - start

    # Display time and solution.
    print('Done!')

    print(f'\nTime elapsed: {time_elapsed}')

    print(f'\nSolution: {solution}')
    print(f'Fitness: {fitness}')
