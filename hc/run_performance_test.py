"""A script for running HC average fitness tests with different
configurations and fitness functions

Author: James Ford
"""

from datetime import datetime

from fitness_functions import *

from hc import simulated_annealing, BenchmarkProblem


def performance_test(num_tests, num_iter):
    """Runs performance test for PSO."""

    # Set the problem.
    p = BenchmarkProblem(
        f=rastrigin_function,
        search_domain=[[-5.12, 5.12]] * 3,
    )

    print('Running SA performance test...')

    results = []
    time_results = []
    for test in range(num_tests):
        print(f'\nTest: {test + 1} ================================')
        results_for_test = []
        time_results_for_test = []
        for iter in range(num_iter):
            start = datetime.now()
            _, fitness = simulated_annealing(
                f=p.fitness_function,
                random_start=p.random_start,
                small_change=p.small_change,
                min_max=-1,
                iterations=100**iter,
                T_0=100,
                T_iter=0.01,
            )
            time_elapsed = datetime.now() - start
            results_for_test.append(fitness)
            time_results_for_test.append(time_elapsed.microseconds)
            print(f'{(10 ** iter) ** 2}: {fitness} ({time_elapsed})')

        results.append(results_for_test)
        time_results.append(time_results_for_test)

    return (
        results,
        time_results,
    )


if __name__ == '__main__':
    performance_test(
        num_tests=10,
        num_iter=4,
    )
