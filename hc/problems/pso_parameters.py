"""A problem class for the PSO parameters problem.

Author: James Ford
"""

from random import randint, uniform

from typing import Callable, List

from hc.problems.base import BaseProblem

from pso import single_objective_optimisation


class PSOParametersProblem(BaseProblem):
    """Problem for configuring PSO for best results."""

    def __init__(
            self,
            f: Callable[[List[float]], float],
            search_domain: List[List[float]],
            min_max: int = -1,
            T: int = 100,
            N: int = 100,
            v_min: int = 0,
            v_max: int = 1,
            p_max: int = 2,
            iterations: int = 100,
    ):
        """Initialises the parameters outside the scope of this
        problem, as well as the maximum value that a parameter may be.

        :param f: The fitness function used to evaluate the particle
            position.
        :param search_domain: The lower/upper bounds of each dimension
            in the search space. e.g. [[-1,1],[-2,2]] for x to be
            between -1 and 1, and y to be between -2 and 2.
        :param min_max: 1 for maximisation problem, -1 for
            minimisation.
        :param T: The number of iterations PSO should run for.
        :param N: The number of particles in the swarm.
        :param p_max: The maximum value that the parameters may be.
        :param iterations: The number of PSO runs to perform when
            calculating the average.
        """

        self.f = f
        self.search_domain = search_domain
        self.min_max = min_max
        self.T = T
        self.N = N
        self.v_min = v_min
        self.v_max = v_max
        self.p_max = p_max
        self.iterations = iterations

    def random_start(self) -> List[float]:
        """Generates a random list of floats, whose values will map to
        the PSO parameters.

        :return: A list of 3 random uniform numbers.
        """

        w = [uniform(0, 1)]
        c1_c2 = [uniform(0, self.p_max) for _ in range(2)]
        return w + c1_c2

    def small_change(self, solution: List[float]) -> List[float]:
        """Chooses a random parameter and modifies it.

        :param solution: The values of the parameters.
        :return: The modified solution.
        """

        # Make a copy of the solution to avoid pointing issues.
        new_sol = solution.copy()

        # Randomly select a parameter.
        i = randint(0, len(solution) - 1)

        # Choose a max value for the parameter, either p_max or
        # 1, in the case of w.
        higher = self.p_max
        if i == 0:
            higher = 1

        # Select a new value, so long as the value is outside the allowed
        # range.
        new_val = -1
        while new_val < 0 or new_val > higher:
            new_val = solution[i] + (uniform(-1, 1))

        # Set the new parameter value.
        new_sol[i] = new_val

        return new_sol

    def fitness_function(self, solution: List[float]) -> float:
        """Performs a series of PSO runs with all parameters and
        calculates the average global best fitness.

        :param solution: The values of the parameters.
        :return: The average global best fitness.
        """

        avg = 0
        for _ in range(self.iterations):
            _, fitness, _ = single_objective_optimisation(
                f=self.f,
                search_domain=self.search_domain,
                min_max=self.min_max,
                T=self.T,
                N=self.N,
                w=solution[0],
                c1=solution[1],
                c2=solution[2],
                v_min=self.v_min,
                v_max=self.v_max,
            )
            avg += fitness

        return avg / self.iterations
