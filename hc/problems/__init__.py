"""A collection of problem class for Hill Climbing and their associated
fitness functions, random start generators and small change operators.

Author: James Ford
"""

from .benchmark import *
from .r import *
from .scales import *
