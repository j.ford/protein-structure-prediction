"""A problem class for the benchmark fitness functions.

Author: James Ford
"""

from random import randint, uniform

from typing import Callable, List

from hc.problems.base import BaseProblem


class BenchmarkProblem(BaseProblem):
    """Required methods for a benchmark fitness function test."""

    def __init__(
            self,
            f: Callable[[List[float]], float],
            search_domain: List[List[float]],
    ):
        """Initialises the fitness function and search domain.

        :param f: The fitness function to evaluate a position.
        :param search_domain: The lower/upper bounds of each dimension
            in the search space. e.g. [[-1,1],[-2,2]] for x to be
            between -1 and 1, and y to be between -2 and 2.
        """
        self.fitness_function = f
        self.search_domain = search_domain

    def random_start(self) -> List[float]:
        """Generates a random position within the search domain.

        :return: The random position.
        """

        return [uniform(d[0], d[1]) for d in self.search_domain]

    def small_change(self, position: List[float]) -> List[float]:
        """Performs a small change to a position.

        :param position: The position to be changed.
        :return: The new position after the change.
        """

        new_pos = [i for i in position]
        i = randint(0, len(position) - 1)
        new_pos[i] += (uniform(-1, 1))
        return new_pos
