"""A problem class for reverse engineering R values to check validity.

Author: James Ford
"""

from random import randint, uniform

from typing import Callable, List

from pso import Particle, get_best_positions_and_fitness

from hc.problems.pso_parameters import PSOParametersProblem


class RProblem(PSOParametersProblem):
    """Problem for configuring PSO for best results."""

    def __init__(
            self,
            f: Callable[[List[float]], float],
            trajectory,
            min_max: int = -1,
            N: int = 265,
            p_max: int = 2,
    ):
        """Initialises the parameters outside the scope of this
        problem, as well as the maximum value that a parameter may be.

        :param f: The fitness function used to evaluate the particle
            position.
        :param trajectory:
        :param min_max: 1 for maximisation problem, -1 for
            minimisation.
        :param N: The number of particles in the swarm.
        :param p_max: The maximum value that the parameters may be.
        """

        super().__init__(f, [[0]*2]*3, min_max, p_max, N)
        self.trajectory = trajectory
        self.best_fitness = None
        self.best_results = None
        self.boundaries = []
        self.best_fitness_per_iter = []

    def fitness_function(self, solution: List[float]) -> float:
        """The fitness function for the R problem, reverse-engineering
        The position of the particles to assess the R values.

        :param solution: The values of the parameters.
        :return: The number of valid r pairs.
        """

        w = solution[0]
        c1 = solution[1]
        c2 = solution[2]

        def find_r2(p: Particle, r1: float):
            return (
                           p.velocity[0]
                           - (p.previous_velocity[0] * w)
                           - (c1 * r1 * (p.best_position[0] - p.position[0]))
                   ) / (
                           c2 * (g_pos[0] - p.position[0])
                   )

        def check_if_valid(lower_bound: float, upper_bound: float):
            return 1 > lower_bound > 0 and 1 > upper_bound > 0

        particles = [Particle(
            search_domain=self.search_domain,
            v_min=0,
            v_max=1,
        ) for _ in range(self.N)]

        g_pos = None
        g_fitness = None

        results = []
        boundaries = []

        for t in range(len(self.trajectory)):
            boundaries_t = []
            t_i_positions = [list(p) for p in self.trajectory[t]]

            for i in range(self.N):
                particles[i].position = t_i_positions[i]

            for p in particles:
                p.best_position, p.best_fitness, g_pos, g_fitness = \
                    get_best_positions_and_fitness(
                        p=p,
                        g_pos=g_pos,
                        g_fitness=g_fitness,
                        f=self.f,
                        min_max=self.min_max,
                    )

                if t > 0:
                    p.velocity = [
                        p.position[i] - p.previous_position[i]
                        for i in range(len(self.search_domain))
                    ]

                if t > 1:
                    lower = find_r2(p, 0)
                    upper = find_r2(p, 1)
                    boundaries_t.append([lower, upper])
                    valid = check_if_valid(lower, upper)
                    results.append(not valid)

                p.previous_position = p.position.copy()
                p.previous_velocity = p.velocity.copy()

            boundaries.append(boundaries_t)

        fitness = sum(results)

        if self.best_fitness is None or fitness < self.best_fitness:
            self.best_fitness = fitness
            self.best_results = results.copy()
            self.boundaries = boundaries.copy()

        self.best_fitness_per_iter.append(self.best_fitness)

        return sum(results)
