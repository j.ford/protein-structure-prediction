"""An interface for hill climbing problem definitions.

Author: James Ford
"""


class BaseProblem:
    """An interface for hill climbing problem definitions."""

    def fitness_function(self, *args, **kwargs):
        pass

    def small_change(self, *args, **kwargs):
        pass

    def random_start(self, *args, **kwargs):
        pass
