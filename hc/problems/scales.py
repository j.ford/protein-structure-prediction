"""A problem class for the scales problem.

Author: James Ford
"""

from random import uniform

from typing import List

from hc.problems.base import BaseProblem


class ScalesProblem(BaseProblem):
    """Required methods for the scales problem."""

    def __init__(self, W: List[float]):
        """Sets the weights needed for each method in the scales
        problem.

        :param W: The list of weights to be organised on the scales.
        """
        self.W = W

    def random_start(self) -> str:
        """ Generates a random solution to the scales problem, based
        on the weights provided.

        :return: The random solution.
        """

        # Initialise the solution as an empty string.
        solution = ''

        # For each weight...
        for i in range(len(self.W)):
            # ...randomly choose a 0 or a 1...
            r = round(uniform(0, 1))
            # ...and add it to the solution.
            solution += str(r)

        return solution

    def small_change(self, solution: str) -> str:
        """Randomly switches one of the numbers in the solution.

        :param solution: The solution to be changed.
        :return: The new solution after the change.
        """

        # Randomly choose a weight to flip.
        r = round(uniform(0, len(self.W)-1))

        # Switch the weight to the other scale.
        change = '0' if solution[r] == '1' else '1'
        solution = solution[0:r] + change + solution[r+1:]

        return solution

    def fitness_function(self, solution: str) -> float:
        """The fitness function for the scales problem. Assigns all
        weights marked with a 0 to the left side and all weights
        marked with a 1 to the right side. The fitness is the absolute
        value of the left weight minus the right weight.

        :param solution: The solution to be assessed.
        :return: The difference in weight between the left and right
            scales.
        """

        # Initialise the left and right weights to 0.
        left = 0
        right = 0

        # For each weight...
        for i in range(len(self.W)):
            # ...if the weight has been assigned a 0...
            if solution[i] == '0':
                # ...add its weight to the left scale...
                left += self.W[i]
            else:
                # ...otherwise, add its weight to the right scale.
                right += self.W[i]

        # Return the difference in weight between the two scales.
        return abs(left - right)
