"""A file of custom exceptions for use in the implementation of
Hill Climbing Optimisation (HC) for "CS3705 - Final Year Project".

Author: James Ford
"""


class HCConfigurationException(Exception):
    """An exception generated when HC has been configured improperly.
    """
    pass
