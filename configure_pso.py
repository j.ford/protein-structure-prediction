"""Finds the parameters for PSO that return the most valid R ranges for
the data.

Author: James Ford
"""

from datetime import datetime

from fitness_functions import *

from hc import simulated_annealing, PSOParametersProblem
from hc.problems.base import BaseProblem


def run(problem: BaseProblem, iterations: int = 100):
    """Finds the parameters for PSO that return the most valid R ranges
    for the data.

    :param problem: The SA problem definition to optimise for.
    :param iterations: The number of iterations SA should run for.
    """

    print('Running SA...')

    # Start the timer.
    start = datetime.now()

    # Run SA...
    solution, fitness = simulated_annealing(
        f=problem.fitness_function,
        random_start=problem.random_start,
        small_change=problem.small_change,
        min_max=-1,
        iterations=iterations,
        T_0=100,
        T_iter=0.01,
    )

    # Record time elapsed.
    time_elapsed = datetime.now() - start

    # Display time and solution.
    print('Done!')

    print(f'\nTime elapsed: {time_elapsed}')

    print(f'\nFitness: {fitness}')
    print(f'w={solution[0]},')
    print(f'c1={solution[1]},')
    print(f'c2={solution[2]},')

    return solution, fitness


if __name__ == '__main__':
    problem = PSOParametersProblem(
        f=rastrigin_function,
        search_domain=[[-5.12, 5.12]] * 3,
        iterations=10,
    )
    run(problem, iterations=10)
