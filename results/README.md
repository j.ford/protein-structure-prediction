# Results files

This directory is where generated results files are saved. It will contain
JSON files that detail both performance data for PSO and SA, as well as the
results from the experimental runs.
